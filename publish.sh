#!/bin/bash
set -e
IFS='|'

CODEGEN="{\
\"generateCode\":true,\
\"codeLanguage\":\"javascript\",\
\"fileNamePattern\":\"src/graphql/**/*.js\",\
\"generatedFileName\":\"\",\
\"docsFilePath\":\"src/graphql\",\
\"generateDocs\":true\
}"

amplify publish \
--codegen $CODEGEN \
--yes